# Created by: Andy Barrios (D@RkArt!s@n)
# This python script is for downloading comics images from http://www.readcomics.tv/
# Please be guided that downloading and distributing it as printed materials is considered as piracy.
# Use at your own risk.

# Open howto.gif in your browser (Google Chrome or Mozilla Firefox)

#How to use this script? Please read instructions below:

# STEP 1: visit http://www.readcomics.tv/x-men-92-2016-1/chapter-1

# STEP 2: Right click on the image and choose inspect element
#         --> https://snag.gy/0n6KxI.jpg (please click the link for screenshot)

# STEP 3: Copy the image url http://www.readcomics.tv/images/manga/x-men-92-2016-1/1
#          NOTE: do not copy the "/1.jpg" string
#         --> https://snag.gy/w2Jvh0.jpg (please click the link for screenshot)

# STEP 4: run this python script in your terminal(linux/mac) or in your cmd(windows)
#         $ python comicsdownloader.py
#         Follow the instructions like creating new folder and pasting your link above
#         http://www.readcomics.tv/images/manga/x-men-92-2016-1/1

# STEP 5: Wait for the script to finish to download all your images



NOTE: Do not open the howto.gif in your image viewer. Open in any browser(mozilla or google chrome)
