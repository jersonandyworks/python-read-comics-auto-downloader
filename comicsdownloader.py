# Created by: Andy Barrios (D@RkArt!s@n)
# This python script is for downloading comics images from http://www.readcomics.tv/
# Please be guided that downloading and distributing it as printed materials is considered as piracy.
# Use at your own risk.

#How to use this script? Please read instructions below:

# STEP 1: visit http://www.readcomics.tv/x-men-92-2016-1/chapter-1

# STEP 2: Right click on the image and choose inspect element
#         --> https://snag.gy/0n6KxI.jpg (please click the link for screenshot)

# STEP 3: Copy the image url http://www.readcomics.tv/images/manga/x-men-92-2016-1/1
#          NOTE: do not copy the "/1.jpg" string
#         --> https://snag.gy/w2Jvh0.jpg (please click the link for screenshot)

# STEP 4: run this python script in your terminal(linux/mac) or in your cmd(windows)
#         $ python comicsdownloader.py
#         Follow the instructions like creating new folder and pasting your link above
#         http://www.readcomics.tv/images/manga/x-men-92-2016-1/1

# STEP 5: Wait for the script to finish to download all your images

import requests
import os

class ComicsAutoDownloader:
    def __init__(self,directoryName):
        self.directoryName = directoryName

def main():
    try:
        directoryName = raw_input("Create new directory for your comics: ")
        link = raw_input("Paste comics url here: ")
        if directoryName:
            ComicsAutoDownloader(directoryName)
            os.mkdir(directoryName)
        else:
            raise ValueError("Directory can not be empty!")
        totalImageCounts = int(raw_input("How many images?: "))
        for i in range(1,totalImageCounts+1):
            print "Downloading "+ str(i) +".jpg to "+ directoryName + "*******"
            r = requests.get(link + '/' + str(i) +'.jpg')
            img = open(directoryName+'/'+ str(i) +'.jpg','w')
            img.write(r.content)
            print "Image: "+ str(i) +".jpg successfully downloaded to "+directoryName
            img.close()

    except TypeError as e:
        print(e)
    except ValueError as ve:
        print(ve)
    except OSError as oe:
        print directoryName + " already exists!"
    else:
        print directoryName + " images has been downloaded successfully!"


if __name__ == '__main__':
    main()